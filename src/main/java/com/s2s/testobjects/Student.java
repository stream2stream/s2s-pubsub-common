/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.testobjects;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Selvyn
 */
public class Student    implements Serializable
{
    private String fname;
    private String lname;
    private Date    dob;

    public  Student(){}
    
    public  Student( String fname, String lname, Date dob )
    {
        this.fname = fname;
        this.lname = lname;
        this.dob = dob;
    }
    
    public String getFname()
    {
        return fname;
    }

    public void setFname(String fname)
    {
        this.fname = fname;
    }

    public String getLname()
    {
        return lname;
    }

    public void setLname(String lname)
    {
        this.lname = lname;
    }

    public Date getDob()
    {
        return dob;
    }

    public void setDob(Date dob)
    {
        this.dob = dob;
    }
    
}
