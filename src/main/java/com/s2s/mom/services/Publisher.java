/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.mom.services;

import com.rabbitmq.client.Channel;
import com.s2s.util.objects.ObjectMemoryMapping;
import java.io.IOException;

/**
 *
 * @author Selvyn
 */
public class Publisher
{
    private final Channel itsChannel;
    private final String itsChannelPath;

    public Publisher(Channel channel, String channelPath)
    {
        itsChannel = channel;
        itsChannelPath = channelPath;
    }

    public Channel getChannel()
    {
        return itsChannel;
    }

    public String getChannelPath()
    {
        return itsChannelPath;
    }

    private byte[]    commonsSetup( Object data )
    {
        ObjectMemoryMapping omm = new ObjectMemoryMapping();
        omm.serializeToMemory(data);
        byte rawdata[] = omm.getRawBytesFromObject();

        ObjectCarrier oc = new ObjectCarrier(data.getClass().getTypeName(), rawdata);
        omm.serializeToMemory(oc);
        rawdata = omm.getRawBytesFromObject();
        
        return rawdata;
    }
    
    public  void sendToQueue(Object data) throws IOException
    {
        byte rawdata[] = commonsSetup( data );
        
        itsChannel.basicPublish("", itsChannelPath, null, rawdata);
    }

    public  void sendToExchange(Object data) throws IOException
    {
        byte rawdata[] = commonsSetup( data );        

        itsChannel.basicPublish(itsChannelPath, "", null, rawdata);
    }
}
