/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.mom.services;

import com.s2s.util.objects.ObjectMemoryMapping;
import java.util.HashMap;

/**
 *
 * @author Selvyn
 */
public  class ObjectReconstructionFactory
{
    private final HashMap<String, ChannelEventHandler> itsEventHandler = new HashMap<>();

    public  void pushEvent(byte[] body)
    {
        ObjectMemoryMapping omm = new ObjectMemoryMapping();
        Object obj = omm.deserializeFromMemory(body);
        ObjectCarrier oc = (ObjectCarrier)obj;
        
        ChannelEventHandler fh = itsEventHandler.get(oc.getType());
        
        if( fh != null )
            fh.handleEvent(oc.reconstituteObject());
    }
    
    public  void    registerHandler( String objectType, ChannelEventHandler fh )
    {
        itsEventHandler.put(objectType, fh);
    }
}
