/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.mom.services;

import com.s2s.util.objects.ObjectMemoryMapping;
import java.io.Serializable;

/**
 *
 * @author Selvyn
 */
public class ObjectCarrier  implements Serializable
{
    private byte    rawObjectAsBytes[];
    private String  objectType;
    
    public  ObjectCarrier(String type, byte objectInBytes[] )
    {
        rawObjectAsBytes = objectInBytes;
        objectType = type;
    }
    
    public  Object  reconstituteObject()
    {
        ObjectMemoryMapping omm = new ObjectMemoryMapping();
        Object result = omm.deserializeFromMemory(rawObjectAsBytes);
        
        return result;
    }
    
    public  String  getType()
    {
        return objectType;
    }
}
