/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.mom.services;

/**
 *
 * @author Selvyn
 */
@FunctionalInterface
public  interface ChannelEventHandler
{
    public  void    handleEvent( Object body );
}
