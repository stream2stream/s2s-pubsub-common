/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.mom.services;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author Selvyn
 */
public class RabbitMQConfig
{

    private final String itsHostID;
    private final String itsExchangeName;
    private Connection itsConnection;
    private Channel itsChannel;
    private Publisher itsPublisher;
    private String  itsQueueName = null;

    public RabbitMQConfig(String hostId, String exchangeName)
    {
        itsHostID = hostId;
        itsExchangeName = exchangeName;
        itsQueueName = exchangeName;
    }

    public String getHostID()
    {
        return itsHostID;
    }

    public String getExchangeName()
    {
        return itsExchangeName;
    }

    public Connection getConnection()
    {
        return itsConnection;
    }

    public Channel getChannel()
    {
        return itsChannel;
    }

    public Publisher getPublisher()
    {
        return itsPublisher;
    }

    public  String  getQueueName()
    {
        return itsQueueName;
    }
    
    private Publisher initMQCommon() throws IOException, TimeoutException
    {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(itsHostID);
        itsConnection = factory.newConnection();
        itsChannel = itsConnection.createChannel();
        
        itsPublisher = new Publisher(itsChannel, itsExchangeName);

        return itsPublisher;
    }   
    
    public Publisher initMQPublisher() throws IOException, TimeoutException
    {
        Publisher pub = initMQCommon();
        itsChannel.exchangeDeclare(itsExchangeName, "fanout");

        return pub;
    }

    public Publisher initMQSubscriber() throws IOException, TimeoutException
    {
        Publisher pub = initMQCommon();
        itsChannel.exchangeDeclare(itsExchangeName, "fanout");
        itsQueueName = itsChannel.queueDeclare().getQueue();
        itsChannel.queueBind(itsQueueName, itsExchangeName, "");
        
        return pub;
    }
    
    public  Publisher   initMQSimpleChannel() throws IOException, TimeoutException
    {
        Publisher pub = initMQCommon();
        itsChannel.queueDeclare(itsQueueName, false, false, false, null);
 
        return pub;
    }

    public void close() throws IOException, TimeoutException
    {
        itsChannel.close();
        itsConnection.close();
    }
}
