/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.util.objects;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Selvyn
 */
@XmlRootElement
public class MessageCarrier
{
    private String  jsonMsg;
    
    public  MessageCarrier(){}
    
    public  MessageCarrier( String msg )
    {
        jsonMsg = msg;
    }
    
    public  String  getMsg()
    {
        return jsonMsg;
    }
}
