/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.util.objects;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Selvyn
 */
public class ObjectMemoryMapping
{
    private static  String encoding = "UTF-8";
    private byte    rawBytesFromObject[] = null;
    private String  buffer;
    private String  utf8String;
    private byte    stringAsBytesAsB64[];
    
    public  byte[]  getStringAsBytesAsB64()
    {
        return stringAsBytesAsB64;
    }
    
    public  String  getStringUTF8()
    {
        return utf8String;
    }
    
    public  byte[]  getRawBytesFromObject()
    {
        return rawBytesFromObject;
    }
    
    public  String  getBuffer()
    {
        return buffer;
    }
    
    public  String    serializeToMemory( Object obj)
    {
        ObjectOutputStream oos = null;
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream( baos );
            oos.writeObject( obj );
            rawBytesFromObject = baos.toByteArray();
            stringAsBytesAsB64 = Base64.encodeBase64(rawBytesFromObject);
            utf8String = StringUtils.newStringUtf8(stringAsBytesAsB64);

            oos.close();
        } catch (IOException ex)
        {
            Logger.getLogger(ObjectMemoryMapping.class.getName()).log(Level.SEVERE, null, ex);
        } finally
        {
            try
            {
                oos.close();
            } catch (IOException ex)
            {
                Logger.getLogger(ObjectMemoryMapping.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return utf8String;
    }
    
    public  Object  deserializeFromMemory()
    {
        return deserializeFromMemory( utf8String );
    }
    
    public  Object    deserializeFromMemory( String utf8StringInput )
    {
        Object result = null;
        ObjectInputStream ois = null;
        try
        {
            byte bytesAsB64[] = Base64.decodeBase64(utf8StringInput);
            ByteArrayInputStream bais = new ByteArrayInputStream(bytesAsB64);
            ois = new ObjectInputStream(bais);
            result = ois.readObject();
        } catch (IOException | ClassNotFoundException ex)
        {
            Logger.getLogger(ObjectMemoryMapping.class.getName()).log(Level.SEVERE, null, ex);
        } finally
        {
            try
            {
                if( ois != null )
                    ois.close();
            } catch (IOException ex)
            {
                Logger.getLogger(ObjectMemoryMapping.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }

    public  Object    deserializeFromMemory( byte rawByteInput[] )
    {
        Object result = null;
        ObjectInputStream ois = null;
        try
        {
            ByteArrayInputStream bais = new ByteArrayInputStream(rawByteInput);
            ois = new ObjectInputStream(bais);
            result = ois.readObject();
        } catch (IOException | ClassNotFoundException ex)
        {
            Logger.getLogger(ObjectMemoryMapping.class.getName()).log(Level.SEVERE, null, ex);
        } finally
        {
            try
            {
                if( ois != null )
                    ois.close();
            } catch (IOException ex)
            {
                Logger.getLogger(ObjectMemoryMapping.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
}
