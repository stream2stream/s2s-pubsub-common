/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.util.objects;

/**
 *
 * @author Selvyn
 */
public class ExceptionMessage   extends TopicMessage
{
    private String  cData;
    private int     exceptionCode;

    public ExceptionMessage()
    {
        super("default topic", "<empty>" );
    }
    
    public  ExceptionMessage( int code, String msg )
    {
        super("default topic", msg );
        exceptionCode = code;
        cData = msg;
    }
    
    public String getCData()
    {
        return cData;
    }

    public void setCData(String data)
    {
        cData = data;
    }
    
    public  int getExceptionCode()
    {
        return exceptionCode;
    }
    
    public  void    setExceptionCode( int code )
    {
        exceptionCode = code;
    }
}
