/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.util.objects;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Selvyn
 */
@XmlRootElement
public class TopicMessage   implements Serializable
{
    private String  topicName;
    private String    data;

    public TopicMessage()
    {
    }

    public TopicMessage(String topic, String msg)
    {
        topicName = topic;
        data = msg;
    }
    
    public  String  getTopicName()
    {
        return topicName;
    }
    
    public  void    setTopicName( String name )
    {
        topicName = name;
    }
    
    public  String  getData()
    {
        return data;
    }
    
    public  void    setData( String data )
    {
        this.data = data;
    }
}
