/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.util.objects;

/**
 *
 * @author Selvyn
 */
public class SimpleErrorMessage
{
    private String  message;
    
    public  SimpleErrorMessage( String msg )
    {
        message = msg;
    }
    
    public  String  getMessage()
    {
        return message;
    }
    
    public  void    setMessage( String msg )
    {
        message = msg;
    }
}
